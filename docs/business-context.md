# Business Context

* Goals
    * Key Concepts:
        * Motivations
    * Questions:
        * Why are we moving the solution to the cloud?
        * What are the business objectives or quanitifiable business goals?
        * What outcomes will you achieve for customer?
        * how will you judge success?
        * Is there a timeline for building the solution the cloud?
        * Is there a targeted event or date for an announcement about the solution's availability on the cloud?
* Customers
    * Key Concepts:
        * Customer profiles, expectations
    * Questions:
        * Is there a deal or customer opportunity associated with having the solution in the cloud?
        * Are customers requesting the solution in the Cloud?
        * How many customers do you have?
        * Are customers willing to work with you to pilot the solution in the cloud?
        * Are your customers already using the cloud?
        * Are customers already comfortable using Cloud?
* Partners
    * Key Concepts:
        * Who are the ISV's partners for professional services?
    * Questions:
        * Do customers work with partners to implement and manage the solution?
        * Who are the partners?
        * What does a typical engagement look like? 
* Industry
    * Key Concepts:
        * Retail
        * Financial Services
        * Healthcare
        * Insurance
        * Federal
    * Questions:
        * What industries does the solution serve?
        * What roles in that industry do customers have (e.g Healthcare: payer, provider, research, pharma, etc)
        * Are there any special industry/compliance security requirements?
        * Are there government regulations that need to be considered?
        * Will the solution be required to run in certain cloud datacenters (Government or Geo specific)?
        * What log retention policies do you have? 
* Model
    * Key Concept:
        * Hosting
    * Questions:
        * Do customers host the solution themselves? 
        * Do you offer a hosting service?
        * If it's a hosted model, where is it hosted?
        * If it's hosted in a competitor cloud, what does the architecuter look like? How much does it cost to host today?
        * Will you be migrating existing customers to the cloud? 
        * Is the cloud solution for new customers only? 
        * Will you continue to onboard customers into your existing hosting environment?
        * Is the cloud only for certain kinds of customers?
* Competitors
    * Key Concepts:
        * Competition, Challenges, Strategy, Differenation
    * Questions:
        * What are the strategic advantages of the solution, compared to competitors?
        * What is the strategy for your solution in the marketplace?
        * Who are the solution's competitors? (be diplomatic)
        * Are you seeing disruption in the market for your solution?
        * Are there new challenges in the market for this solution?
        * Are customers finding new "substitutes" for your solution?

* Licensing
    * Key Concepts:
      * SAAS vs Perpetual License, Tiers, 
    * Questions:
        * How do you license the solution today?
        * How is the solution sold?
        * Going forward, will this become a cloud/SAAS only solution?
        * Is the solution delivered in different offerings or tiers?
        * Are there t-shirt sized offerings for the solution?
        * Are there different support/BCDR levels per solution tier?
        * What is the typical sales cycle for the solution?
        * What are the pain points in the sales cycle?
        * How will moving to the cloud affect the sales cycle?
        * Is this an internal line of busines solution or is it external facing? 
        * Do your customers rely on the solution to provide services to their own end-customers?
